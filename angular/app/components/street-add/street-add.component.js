class StreetAddController{
    constructor (API, $state, $stateParams) {
        'ngInject'

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []

        let City = this.API.service('city')

        City.getList()
            .then((response) => {
                this.cities = response.plain();
            });

        let District = this.API.service('district')

        District.getList()
            .then((response) => {
                this.districts = response.plain();
            });

        if ($stateParams.alerts) {
            this.alerts.push($stateParams.alerts)
        }
    }

    save (isValid) {
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
            let Street = this.API.service('add', this.API.all('street'))
            let $state = this.$state

            Street.post({
                'name': this.name,
                'district_id': this.district.id
            }).then(function () {
                let alert = { type: 'success', 'title': 'Success!', msg: 'Street has been added.' }
                $state.go($state.current, { alerts: alert})
            }, function (response) {
                let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                $state.go($state.current, { alerts: alert})
            })
        } else {
            this.formSubmitted = true
        }
    }

    $onInit () {}
}

export const StreetAddComponent = {
    templateUrl: './views/app/components/street-add/street-add.component.html',
    controller: StreetAddController,
    controllerAs: 'vm',
    bindings: {}
}
