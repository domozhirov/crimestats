class DistrictAddController{
    constructor (API, $state, $stateParams) {
        'ngInject'

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []

        let City = this.API.service('city')

        City.getList()
            .then((response) => {
                this.cities = response.plain();
                this.city = this.cities[0];
            });

        if ($stateParams.alerts) {
            this.alerts.push($stateParams.alerts)
        }
    }

    save (isValid) {
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
            let District = this.API.service('add', this.API.all('district'))
            let $state = this.$state

            District.post({
                'name': this.name,
                'city_id': this.city.id
            }).then(function () {
                let alert = { type: 'success', 'title': 'Success!', msg: 'District has been added.' }
                $state.go($state.current, { alerts: alert})
            }, function (response) {
                let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                $state.go($state.current, { alerts: alert})
            })
        } else {
            this.formSubmitted = true
        }
    }

    $onInit () {}
}

export const DistrictAddComponent = {
    templateUrl: './views/app/components/district-add/district-add.component.html',
    controller: DistrictAddController,
    controllerAs: 'vm',
    bindings: {}
}
