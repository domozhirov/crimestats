class StreetEditController{
    constructor ($stateParams, $state, API) {
        'ngInject'

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []

        if ($stateParams.alerts) {
            this.alerts.push($stateParams.alerts)
        }

        let streetId = $stateParams.streetId
        let Street = API.service('show', API.all('street'))

        Street.one(streetId).get()
            .then((response) => {
                this.street = API.copy(response)
            })

        let District = API.service('district')

        District.getList()
            .then((response) => {
                this.districts = response.plain()
                this.district = this.districts[0] || null
            });
    }

    save (isValid) {
        if (isValid) {
            let $state = this.$state
            this.street.put()
                .then(() => {
                    let alert = { type: 'success', 'title': 'Success!', msg: 'Street has been updated.' }
                    $state.go($state.current, { alerts: alert})
                }, (response) => {
                    let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                    $state.go($state.current, { alerts: alert})
                })
        } else {
            this.formSubmitted = true
        }
    }
}

export const StreetEditComponent = {
    templateUrl: './views/app/components/street-edit/street-edit.component.html',
    controller: StreetEditController,
    controllerAs: 'vm',
    bindings: {}
}
