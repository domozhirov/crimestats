class DistrictListController {
    constructor ($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
        'ngInject';
        this.API = API
        this.$state = $state
        this.DTOptionsBuilder = DTOptionsBuilder
        this.DTColumnBuilder = DTColumnBuilder
        this.dtInstance = null;
        this.District = API.service('district')
        this.$compile = $compile
        this.$scope = $scope

        let City = this.API.service('city');

        this.dataSet = []

        City.getList()
            .then((response) => {
                this.cities = response.plain()

                this.cities.unshift({
                    id: 0,
                    name: ''
                })

                this.city = this.cities[0]

                this.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('name').withTitle('Name'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        .renderWith(actionsHtml)
                ]


                this.dtOptions = this.DTOptionsBuilder.newOptions()
                    .withOption('data', this.dataSet)
                    .withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()

            })

        let actionsHtml = (data) => {
            return `
                <a class="btn btn-xs btn-warning" ui-sref="app.districtedit({districtId: ${data.id}})">
                    <i class="fa fa-edit"></i>
                </a>
                &nbsp
                <button class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                    <i class="fa fa-trash-o"></i>
                </button>`
        }

        let createdRow = (row) => {
            this.$compile(angular.element(row).contents())(this.$scope)
        }
    }

    change() {
        let DTColumnBuilder = this.DTColumnBuilder
        let API = this.API;
        let District = API.service('list', API.all('district'))

        let cityId = this.city.id

        District.one(cityId).get()
            .then((response) => {
                this.dataSet = response.data.district;


                this.displayTable = true
                console.log(this.dtInstance, this.dataSet);
                if (this.dtInstance) {
                    this.dtInstance.reloadData();
                }
            });
    }

    delete (districtId) {
        let API = this.API
        let $state = this.$state

        swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this data!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            html: false
        }, function () {
            API.one('district').one('district', districtId).remove()
                .then(() => {
                    swal({
                        title: 'Deleted!',
                        text: 'District has been deleted.',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                        $state.reload()
                    })
                })
        })
    }

    $onInit () {}
}

export const DistrictListComponent = {
    templateUrl: './views/app/components/district-list/district-list.component.html',
    controller: DistrictListController,
    controllerAs: 'vm',
    bindings: {}
}
