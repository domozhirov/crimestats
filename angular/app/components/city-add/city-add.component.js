class CityAddController {
    constructor (API, $state, $stateParams) {
        'ngInject'

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []

        if ($stateParams.alerts) {
            this.alerts.push($stateParams.alerts)
        }
    }

    save (isValid) {
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
            let Cities = this.API.service('add', this.API.all('city'))
            let $state = this.$state

            Cities.post({
                'name': this.name
            }).then(function () {
                let alert = { type: 'success', 'title': 'Success!', msg: 'City has been added.' }
                $state.go($state.current, { alerts: alert})
            }, function (response) {
                let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                $state.go($state.current, { alerts: alert})
            })
        } else {
            this.formSubmitted = true
        }
    }

    $onInit () {}
}

export const CityAddComponent = {
    templateUrl: './views/app/components/city-add/city-add.component.html',
    controller: CityAddController,
    controllerAs: 'vm',
    bindings: {}
}
