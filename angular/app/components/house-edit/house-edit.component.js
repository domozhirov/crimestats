class HouseEditController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const HouseEditComponent = {
    templateUrl: './views/app/components/house-edit/house-edit.component.html',
    controller: HouseEditController,
    controllerAs: 'vm',
    bindings: {}
}
