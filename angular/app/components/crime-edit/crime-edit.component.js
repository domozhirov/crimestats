class CrimeEditController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const CrimeEditComponent = {
    templateUrl: './views/app/components/crime-edit/crime-edit.component.html',
    controller: CrimeEditController,
    controllerAs: 'vm',
    bindings: {}
}
