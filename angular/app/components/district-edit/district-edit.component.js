class DistrictEditController{
    constructor ($stateParams, $state, API) {
        'ngInject'

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []

        if ($stateParams.alerts) {
            this.alerts.push($stateParams.alerts)
        }

        let districtId = $stateParams.districtId
        let District = API.service('show', API.all('district'))
        District.one(districtId).get()
            .then((response) => {
                this.district = API.copy(response)
            })

        let City = API.service('city')

        City.getList()
            .then((response) => {
                this.cities = response.plain()
                this.city = this.cities[0] || null
            });
    }

    save (isValid) {
        if (isValid) {
            let $state = this.$state
            this.district.put()
                .then(() => {
                    let alert = { type: 'success', 'title': 'Success!', msg: 'City has been updated.' }
                    $state.go($state.current, { alerts: alert})
                }, (response) => {
                    let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                    $state.go($state.current, { alerts: alert})
                })
        } else {
            this.formSubmitted = true
        }
    }
}

export const DistrictEditComponent = {
    templateUrl: './views/app/components/district-edit/district-edit.component.html',
    controller: DistrictEditController,
    controllerAs: 'vm',
    bindings: {}
}
