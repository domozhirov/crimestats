class HouseAddController{
    constructor (API, $state, $stateParams) {
        'ngInject'

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []

        let Street = this.API.service('street')

        Street.getList()
            .then((response) => {
                this.streets = response.plain();
                this.street = this.streets[0];
            });

        if ($stateParams.alerts) {
            this.alerts.push($stateParams.alerts)
        }
    }

    save (isValid) {
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
            let House = this.API.service('add', this.API.all('house'))
            let $state = this.$state

            House.post({
                'number': this.number,
                'street_id': this.street.id
            }).then(function () {
                let alert = { type: 'success', 'title': 'Success!', msg: 'House has been added.' }
                $state.go($state.current, { alerts: alert})
            }, function (response) {
                let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                $state.go($state.current, { alerts: alert})
            })
        } else {
            this.formSubmitted = true
        }
    }

    $onInit () {}
}

export const HouseAddComponent = {
    templateUrl: './views/app/components/house-add/house-add.component.html',
    controller: HouseAddController,
    controllerAs: 'vm',
    bindings: {}
}
