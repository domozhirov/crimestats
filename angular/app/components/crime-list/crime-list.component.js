class CrimeListController{
    constructor ($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
        'ngInject';
        this.API = API
        this.$state = $state

        let Crime = this.API.service('crime')

        Crime.getList()
            .then((response) => {
                let dataSet = response.plain()
                this.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('data', dataSet)
                    .withOption('createdRow', createdRow)
                    .withOption('responsive', true)
                    .withBootstrap()

                this.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('name').withTitle('Name'),
                    DTColumnBuilder.newColumn(null).withTitle('Address').renderWith(address),
                    DTColumnBuilder.newColumn('type').withTitle('Type'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').renderWith(isSolvet),
                    DTColumnBuilder.newColumn('article').withTitle('Article'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                        .renderWith(actionsHtml)
                ]

                this.displayTable = true
            })

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
            return `
                <a class="btn btn-xs btn-warning" ui-sref="app.districtedit({districtId: ${data.id}})">
                    <i class="fa fa-edit"></i>
                </a>
                &nbsp
                <button class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                    <i class="fa fa-trash-o"></i>
                </button>`
        }

        let isSolvet = (data) => {
            let opt = data.solved ? {
                label: 'success',
                message: 'solved'
            } : {
                label: 'warning',
                message: 'is not solved'
            };

            return `
                <span class="label label-${opt.label}">${opt.message}</span>            
            `
        }

        let address = (data) => {
            return data.house ?
                `${data.house.number}` :
                `house id undefined`;
        }
    }

    delete (crimeId) {
        let API = this.API
        let $state = this.$state

        swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this data!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            html: false
        }, function () {
            API.one('crime').one('crime', crimeId).remove()
                .then(() => {
                    swal({
                        title: 'Deleted!',
                        text: 'Crime has been deleted.',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                        $state.reload()
                    })
                })
        })
    }

    $onInit () {}
}

export const CrimeListComponent = {
    templateUrl: './views/app/components/crime-list/crime-list.component.html',
    controller: CrimeListController,
    controllerAs: 'vm',
    bindings: {}
}
