class CrimeViewController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const CrimeViewComponent = {
    templateUrl: './views/app/components/crime-view/crime-view.component.html',
    controller: CrimeViewController,
    controllerAs: 'vm',
    bindings: {}
}
