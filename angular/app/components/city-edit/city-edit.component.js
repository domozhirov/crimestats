class CityEditController{
    constructor ($stateParams, $state, API) {
        'ngInject'

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []

        if ($stateParams.alerts) {
            this.alerts.push($stateParams.alerts)
        }

        let cityId = $stateParams.cityId
        let City = API.service('show', API.all('city'))
        City.one(cityId).get()
            .then((response) => {
                this.city = API.copy(response)
            })
    }

    save (isValid) {
        if (isValid) {
            let $state = this.$state
            this.city.put()
                .then(() => {
                    let alert = { type: 'success', 'title': 'Success!', msg: 'City has been updated.' }
                    $state.go($state.current, { alerts: alert})
                }, (response) => {
                    let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                    $state.go($state.current, { alerts: alert})
                })
        } else {
            this.formSubmitted = true
        }
    }
}

export const CityEditComponent = {
    templateUrl: './views/app/components/city-edit/city-edit.component.html',
    controller: CityEditController,
    controllerAs: 'vm',
    bindings: {}
}
