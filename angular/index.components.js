import {CrimeViewComponent} from './app/components/crime-view/crime-view.component';
import {CrimeAddComponent} from './app/components/crime-add/crime-add.component';
import {CrimeEditComponent} from './app/components/crime-edit/crime-edit.component';
import {CrimeListComponent} from './app/components/crime-list/crime-list.component';
import {HouseEditComponent} from './app/components/house-edit/house-edit.component';
import {HouseAddComponent} from './app/components/house-add/house-add.component';
import {HouseListComponent} from './app/components/house-list/house-list.component';
import {StreetEditComponent} from './app/components/street-edit/street-edit.component';
import {StreetAddComponent} from './app/components/street-add/street-add.component';
import { StreetListComponent } from './app/components/street-list/street-list.component';
import { DistrictEditComponent } from './app/components/district-edit/district-edit.component';
import { DistrictAddComponent } from './app/components/district-add/district-add.component';
import { DistrictListComponent } from './app/components/district-list/district-list.component';
import { CityEditComponent } from './app/components/city-edit/city-edit.component';
import { CityAddComponent } from './app/components/city-add/city-add.component';
import { CityComponent } from './app/components/city/city.component';
import { TablesSimpleComponent } from './app/components/tables-simple/tables-simple.component'
import { UiModalComponent } from './app/components/ui-modal/ui-modal.component'
import { UiTimelineComponent } from './app/components/ui-timeline/ui-timeline.component'
import { UiButtonsComponent } from './app/components/ui-buttons/ui-buttons.component'
import { UiIconsComponent } from './app/components/ui-icons/ui-icons.component'
import { UiGeneralComponent } from './app/components/ui-general/ui-general.component'
import { FormsGeneralComponent } from './app/components/forms-general/forms-general.component'
import { ChartsChartjsComponent } from './app/components/charts-chartjs/charts-chartjs.component'
import { WidgetsComponent } from './app/components/widgets/widgets.component'
import { UserProfileComponent } from './app/components/user-profile/user-profile.component'
import { UserVerificationComponent } from './app/components/user-verification/user-verification.component'
import { ComingSoonComponent } from './app/components/coming-soon/coming-soon.component'
import { UserEditComponent } from './app/components/user-edit/user-edit.component'
import { UserPermissionsEditComponent } from './app/components/user-permissions-edit/user-permissions-edit.component'
import { UserPermissionsAddComponent } from './app/components/user-permissions-add/user-permissions-add.component'
import { UserPermissionsComponent } from './app/components/user-permissions/user-permissions.component'
import { UserRolesEditComponent } from './app/components/user-roles-edit/user-roles-edit.component'
import { UserRolesAddComponent } from './app/components/user-roles-add/user-roles-add.component'
import { UserRolesComponent } from './app/components/user-roles/user-roles.component'
import { UserListsComponent } from './app/components/user-lists/user-lists.component'
import { DashboardComponent } from './app/components/dashboard/dashboard.component'
import { NavSidebarComponent } from './app/components/nav-sidebar/nav-sidebar.component'
import { NavHeaderComponent } from './app/components/nav-header/nav-header.component'
import { LoginLoaderComponent } from './app/components/login-loader/login-loader.component'
import { ResetPasswordComponent } from './app/components/reset-password/reset-password.component'
import { ForgotPasswordComponent } from './app/components/forgot-password/forgot-password.component'
import { LoginFormComponent } from './app/components/login-form/login-form.component'
import { RegisterFormComponent } from './app/components/register-form/register-form.component'

angular.module('app.components')
	.component('crimeView', CrimeViewComponent)
	.component('crimeAdd', CrimeAddComponent)
	.component('crimeEdit', CrimeEditComponent)
	.component('crimeList', CrimeListComponent)
	.component('houseEdit', HouseEditComponent)
	.component('houseAdd', HouseAddComponent)
	.component('houseList', HouseListComponent)
	.component('streetEdit', StreetEditComponent)
	.component('streetAdd', StreetAddComponent)
	.component('streetList', StreetListComponent)
	.component('districtEdit', DistrictEditComponent)
	.component('districtAdd', DistrictAddComponent)
	.component('districtList', DistrictListComponent)
	.component('cityEdit', CityEditComponent)
	.component('cityAdd', CityAddComponent)
	.component('city', CityComponent)
    .component('tablesSimple', TablesSimpleComponent)
    .component('uiModal', UiModalComponent)
    .component('uiTimeline', UiTimelineComponent)
    .component('uiButtons', UiButtonsComponent)
    .component('uiIcons', UiIconsComponent)
    .component('uiGeneral', UiGeneralComponent)
    .component('formsGeneral', FormsGeneralComponent)
    .component('chartsChartjs', ChartsChartjsComponent)
    .component('widgets', WidgetsComponent)
    .component('userProfile', UserProfileComponent)
    .component('userVerification', UserVerificationComponent)
    .component('comingSoon', ComingSoonComponent)
    .component('userEdit', UserEditComponent)
    .component('userPermissionsEdit', UserPermissionsEditComponent)
    .component('userPermissionsAdd', UserPermissionsAddComponent)
    .component('userPermissions', UserPermissionsComponent)
    .component('userRolesEdit', UserRolesEditComponent)
    .component('userRolesAdd', UserRolesAddComponent)
    .component('userRoles', UserRolesComponent)
    .component('userLists', UserListsComponent)
    .component('dashboard', DashboardComponent)
    .component('navSidebar', NavSidebarComponent)
    .component('navHeader', NavHeaderComponent)
    .component('loginLoader', LoginLoaderComponent)
    .component('resetPassword', ResetPasswordComponent)
    .component('forgotPassword', ForgotPasswordComponent)
    .component('loginForm', LoginFormComponent)
    .component('registerForm', RegisterFormComponent)
