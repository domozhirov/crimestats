<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crime extends Model
{
    protected $fillable = [
        'name', 'house_id', 'type', 'solved', 'article', 'description',
    ];

    public function house()
    {
        return $this->hasOne(House::class, 'id', 'house_id');
    }
}
