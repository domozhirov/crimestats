<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = [
        'name', 'city_id',
    ];

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
