<?php

namespace App\Http\Controllers;

use App\City;
use App\House;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Input;
use Validator;

class HouseController extends Controller
{
    /**
     * Get all cities.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $house = House::with('street')->get();

        return response()->success(compact('house'));
    }

    /**
     * Add new districts.
     *
     * @return JSON
     */
    public function postAdd()
    {
        $house = House::create([
            'number'    => Input::get('number'),
            'street_id' => Input::get('street_id'),
        ]);

        error_log(var_export($house, 1));

        return response()->success(compact('house'));
    }

    /**
     * Get user details referenced by id.
     *
     * @param int House ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $street = House::find($id);

        return response()->success($street);
    }

    /**
     * Update districts data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $streetForm = array_dot(
            app('request')->only(
                'data.number',
                'data.id'
            )
        );

        $streetId = intval($streetForm['data.id']);

        $streets = House::find($streetId);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.number' => 'required|integer',
        ]);

        $streetsData = [
            'number' => $streetForm['data.number'],
        ];

        $affectedRows = House::where('id', '=', $streetId)->update($streetsData);

        return response()->success('success');
    }

    /**
     * Delete districts by id.
     *
     * @param int districts ID
     *
     * @return JSON
     */
    public function deleteHouse($id)
    {
        House::destroy($id);

        return response()->success('success');
    }
}
