<?php

namespace App\Http\Controllers;

use App\City;
use App\Street;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Input;
use Validator;

class StreetController extends Controller
{
    /**
     * Get all cities.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $street = Street::with('district')->get();

        return response()->success(compact('street'));
    }

    /**
     * Add new districts.
     *
     * @return JSON
     */
    public function postAdd()
    {
        $street = Street::create([
            'name'    => Input::get('name'),
            'district_id' => Input::get('district_id'),
        ]);

        return response()->success(compact('street'));
    }

    /**
     * Get user details referenced by id.
     *
     * @param int Street ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $street = Street::find($id);

        return response()->success($street);
    }

    /**
     * Update districts data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $streetForm = array_dot(
            app('request')->only(
                'data.name',
                'data.id'
            )
        );

        $streetId = intval($streetForm['data.id']);

        $streets = Street::find($streetId);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3',
        ]);

        $streetsData = [
            'name' => $streetForm['data.name'],
        ];

        $affectedRows = Street::where('id', '=', $streetId)->update($streetsData);

        return response()->success('success');
    }

    /**
     * Delete districts by id.
     *
     * @param int districts ID
     *
     * @return JSON
     */
    public function deleteStreet($id)
    {
        Street::destroy($id);

        return response()->success('success');
    }
}
