<?php

namespace App\Http\Controllers;

use App\City;
use App\District;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Input;
use Validator;

class DistrictController extends Controller
{
    /**
     * Get all districts.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $district = District::with('city')->get();

        return response()->success(compact('district'));
    }

    /**
     * Add new districts.
     *
     * @return JSON
     */
    public function postAdd()
    {
        $district = District::create([
            'name'    => Input::get('name'),
            'city_id' => Input::get('city_id'),
        ]);

        return response()->success(compact('district'));
    }

    /**
     * Get user details referenced by id.
     *
     * @param int District ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $district = District::find($id);

        return response()->success($district);
    }

    public function getList($cityId)
    {
        $district = District::where('city_id', '=', $cityId)->get();

        error_log(var_export($district, 1));

        return response()->success(compact('district'));
    }

    /**
     * Update districts data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $districtForm = array_dot(
            app('request')->only(
                'data.name',
                'data.id'
            )
        );

        $districtId = intval($districtForm['data.id']);

        $districts = District::find($districtId);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3',
        ]);

        $districtsData = [
            'name' => $districtForm['data.name'],
        ];

        $affectedRows = District::where('id', '=', $districtId)->update($districtsData);

        return response()->success('success');
    }

    /**
     * Delete districts by id.
     *
     * @param int districts ID
     *
     * @return JSON
     */
    public function deleteDistrict($id)
    {
        District::destroy($id);

        return response()->success('success');
    }
}
