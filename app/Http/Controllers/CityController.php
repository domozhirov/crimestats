<?php

namespace App\Http\Controllers;

use App\City;
use Auth;
use Bican\Roles\Models\Permission;
use Bican\Roles\Models\Role;
use Hash;
use Illuminate\Http\Request;
use Input;
use Validator;

class CityController extends Controller
{
    /**
     * Get all cities.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $city = City::all();

        return response()->success(compact('city'));
    }

    /**
     * Add new city.
     *
     * @return JSON
     */
    public function postAdd()
    {
        $city = City::create([
            'name' => Input::get('name'),
        ]);

        return response()->success(compact('city'));
    }

    /**
     * Get user details referenced by id.
     *
     * @param int City ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $city = City::find($id);

        return response()->success($city);
    }

    /**
     * Update city data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $cityForm = array_dot(
            app('request')->only(
                'data.name',
                'data.id'
            )
        );

        $cityId = intval($cityForm['data.id']);

        $city = City::find($cityId);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3',
        ]);

        $cityData = [
            'name' => $cityForm['data.name'],
        ];

        $affectedRows = City::where('id', '=', $cityId)->update($cityData);

        return response()->success('success');
    }

    /**
     * Delete city by id.
     *
     * @param int City ID
     *
     * @return JSON
     */
    public function deleteCity($id)
    {
        City::destroy($id);

        return response()->success('success');
    }
}
