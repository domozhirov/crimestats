<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    protected $fillable = [
        'number', 'street_id',
    ];

    public function street()
    {
        return $this->hasOne(Street::class, 'id', 'street_id');
    }
}
