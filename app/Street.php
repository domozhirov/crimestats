<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    protected $fillable = [
        'name', 'district_id',
    ];

    public function district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }
}
