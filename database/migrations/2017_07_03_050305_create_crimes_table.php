<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('crimes', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('object_id');
			$table->enum('type', ['criminal', 'administrative']);
			$table->tinyInteger('solved');
			$table->integer('article');
			$table->text('description');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('crimes');
    }
}
